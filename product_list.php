<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Product List</title>
    <!--de script zorgt ervoor dat de icons kunnen worden geladen. De linkjes zorgen ervoor dat css word ingeladen-->
    <script src="https://kit.fontawesome.com/07996627dc.js"></script>
</head>
<body>
<!--Dit is de de header die zorgt ervoor dat het grijze gedeelte boven aan de pagina er komt.-->
<?php
include "header.php";
?>
<!--dit is de zoek term met de select de zoekterm is de titel en de filter staat rechts hiermee kun je filteren op prijs-->
    <h1 class="zoekterm">Naam zoekterm</h1>
    <select class="filter">
        <option value="Prijs laag-hoog">Prijs laag-hoog</option>
        <option value="Prijs hoog-laag">Prijs hoog-laag</option>
        <option value="Verschijndatum">Verschijndatum</option>
    </select>
<!--hier komt het aantal resultaten te staan van je product-->
    <p>Aantal resultaten</p>

<!--dit is 1 grote div waar je product in staat met een image, titel, voorraad, prijs en de icons-->
    <div class="products">
        <div class="product">
            <!--dit is een div voor de image-->
            <div class="img">
                <img src="https://i.ytimg.com/vi/lVgTTAukMOU/maxresdefault.jpg">
            </div>
            <!--dit is een div voor de titel-->
            <div class="content">
                <div class="title">
                    <h5>Banaan</h5>
                </div>
                <!--dit is een div voor de voraad-->
                <div class="vooraad">
                    <h5>7 bananen op vooraad</h5>
                </div>
                <!--dit is een div voor de prijs-->
                <div class="price">
                    <h5>&euro;20</h5>
                </div>
                <!--dit is een div voor de icons-->
                <div class="cart">
                    <a><i class="icon heart far fa-heart"></i></a>
                    <a><i class="icon fas fa-cart-plus"></i></a>
                </div>
            </div>
        </div>
        <div class="product">
            <!--dit is een div voor de image-->
            <div class="img">
                <img src="https://i.ytimg.com/vi/lVgTTAukMOU/maxresdefault.jpg">
            </div>
            <!--dit is een div voor de titel-->
            <div class="content">
                <div class="title">
                    <h5>Banaan</h5>
                </div>
                <!--dit is een div voor de voraad-->
                <div class="vooraad">
                    <h5>7 bananen op vooraad</h5>
                </div>
                <!--dit is een div voor de prijs-->
                <div class="price">
                    <h5>&euro;20</h5>
                </div>
                <!--dit is een div voor de icons-->
                <div class="cart">
                    <a><i class="icon heart far fa-heart"></i></a>
                    <a><i class="icon fas fa-cart-plus"></i></a>
                </div>
            </div>
        </div>
        <div class="product">
            <!--dit is een div voor de image-->
            <div class="img">
                <img src="https://i.ytimg.com/vi/lVgTTAukMOU/maxresdefault.jpg">
            </div>
            <!--dit is een div voor de titel-->
            <div class="content">
                <div class="title">
                    <h5>Banaan</h5>
                </div>
                <!--dit is een div voor de voraad-->
                <div class="vooraad">
                    <h5>7 bananen op vooraad</h5>
                </div>
                <!--dit is een div voor de prijs-->
                <div class="price">
                    <h5>&euro;20</h5>
                </div>
                <!--dit is een div voor de icons-->
                <div class="cart">
                    <a><i class="icon heart far fa-heart"></i></a>
                    <a><i class="icon fas fa-cart-plus"></i></a>
                </div>
            </div>
        </div>
        <div class="product">
            <!--dit is een div voor de image-->
            <div class="img">
                <img src="https://i.ytimg.com/vi/lVgTTAukMOU/maxresdefault.jpg">
            </div>
            <!--dit is een div voor de titel-->
            <div class="content">
                <div class="title">
                    <h5>Banaan</h5>
                </div>
                <!--dit is een div voor de voraad-->
                <div class="vooraad">
                    <h5>7 bananen op vooraad</h5>
                </div>
                <!--dit is een div voor de prijs-->
                <div class="price">
                    <h5>&euro;20</h5>
                </div>
                <!--dit is een div voor de icons-->
                <div class="cart">
                    <a><i class="icon heart far fa-heart"></i></a>
                    <a><i class="icon fas fa-cart-plus"></i></a>
                </div>
            </div>
        </div>
    </div>
<!--dit is een input field waar de gebruiker kaan aangeven hoeveel producten die wilt hebben-->
<input class="amount" type="text" id="myInput" onkeyup="myFunction()" placeholder="products">



</body>
</html>