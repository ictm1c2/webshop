<!--<html>-->
<!--<head>-->
<!--    <title>viewProduct</title>-->
<!--    <script src="https://kit.fontawesome.com/07996627dc.js"></script>-->
<!--    <link rel="stylesheet" type="text/css" href="product.css">-->
<!--</head>-->
<!--<body>-->
<?php
$title = "viewProduct";
include "header.php";
?>
    <div class = "product">
        <section>
            <img src = "assets/images/MV7N2.jfif" width="200" height="200">
            <p class = "omschrijving">Door dit product wordt uw leven veel makkelijker</p>
        </section>
        <section>
            <p class = "titel">Airpods 2019 de luxe</p>
            <div class = "filmpje">
            <iframe class = "filmpje" src="https://www.youtube.com/embed/rtNIQdLpodI?autoplay=1" height="240" width="320" controls>Video not supported</iframe>
            <p class ="leuk">Misschien vind u dit product ook leuk</p>
            </div>
        </section>
        <section>
            <div class = "gegevens">
                <div class="prijs">
                    <p>prijs</p>
                </div>
                <div class = "voorraad">
                    <p>voorraad</p>
                </div>
                <div class="icons">
                    <a href="#"><i class="fas fa-shopping-cart"></i></a>
                    <a href="#"><i class="fas fa-heart"></i></a>
                </div>
            </div>
        </section>
    </div>

<?php
include 'footer.php';
?>