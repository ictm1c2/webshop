<?php include 'includes/autoload.inc.php'; ?>
<html>
<head>
    <title><?php echo $title;?></title>
    <script src="https://kit.fontawesome.com/07996627dc.js"></script>
    <link rel="stylesheet" type="text/css" href="css/header.css">
    <link rel="stylesheet" type="text/css" href="css/product.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/header.css">
</head>
<body>
<nav>
    <div class="top">
        <img src="assets/images/kisspng-logo-web-design-augmented-reality-surface-icon-5b50a28a175192.7606479115320111460955.jpg">
        <div class="search">
            <label for="zoekterm"><i class="fas fa-search"></i></label>
            <input type="search" name="zoekterm" placeholder="search" id="zoekterm">
        </div>
        <div class="acc">
            <p>Hallo, naam</p>
            <a href="#"><i class="fas fa-user-circle"></i></a>
            <a href="#"><i class="fas fa-heart"></i></i></a>
            <a href="#"><i class="fas fa-shopping-cart"></i></i></a>
        </div>
    </div>
    <ul>
        <li><a href="#">categorie 1</a></li>
        <li><a href="#">categorie 2</a></li>
        <li><a href="#">categorie 3</a></li>
        <li><a href="#">categorie 4</a></li>
        <li><a href="#">categorie 5</a></li>
        <li><a href="#">categorie 6</a></li>
        <li><a href="#">categorie 7</a></li>
        <li><a href="#">categorie 8</a></li>
        <li><a href="#">categorie 9</a></li>
        <li><a href="#">categorie 10</a></li>
    </ul>
</nav>
<main>

