<?php

require_once __DIR__ . "/../vendor/autoload.php";

$mollie = new \Mollie\Api\MollieApiClient();
$mollie->setApiKey(Settings::getAPIKey());