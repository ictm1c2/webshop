<?php

try {
    require 'initmollie.inc.php';
    /*
     * Retrieve the payment's current state.
     */
    $payment = $mollie->payments->get($_POST["id"]);
    $orderId = $payment->metadata->order_id;

    $order = Order::fromId($orderId);

    if ($payment->isPaid() && !$payment->hasRefunds() && !$payment->hasChargebacks()) {
        $order->updateStatus(0);
        User::fromId($order->userId)->clearShoppingCart();
    } 
    
    elseif ($payment->isOpen()) {
        $order->updateStatus(1);
    } 
    
    elseif ($payment->isPending()) {
        $order->updateStatus(2);
    } 
    
    elseif ($payment->isFailed()) {
        $order->updateStatus(3);
    } 
    
    elseif ($payment->isExpired()) {
        $order->updateStatus(4);
    } 
    
    elseif ($payment->isCanceled()) {
        $order->updateStatus(5);
    }
} catch (\Mollie\Api\Exceptions\ApiException $e) {
    echo "API call failed: " . htmlspecialchars($e->getMessage());
}