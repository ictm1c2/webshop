<?php

/*
    Only load classes if they are required
*/
function loadClass($class) {
    $path = realpath(__DIR__ . '/../classes/');
    include_once($path . '/' . $class .'.class.php');
}

// spl_autoload_register('loadClass');

include_once(__DIR__ .'/../classes/categorie.class.php');
include_once(__DIR__ .'/../classes/database.class.php');
include_once(__DIR__ .'/../classes/product.class.php');
include_once(__DIR__ .'/../classes/settings.class.php');
