<?php 

/*
    Configuration file, only change database settings if required
    Set debug to true if you want to recieve debug messages
*/
$config = array(
    "database" => array(
        "host" => "localhost",
        "username" => "root",
        "password" => "password",
        "database" => "wwi",
        "charset" => 'utf8mb4',
        "itemTable" => "stockitems",
        "groupTable" => "stockgroups",
        "itemgroupTable" => "stockitemstockgroups",
        "inventoryTable" => "stockitemholdings",
        "userTable" => "users",
        "shoppingCartTable" => "shoppingcart",
        "wishlistTable" => "wishlist",
        "ordersTable" => "orders",
        "orderItemTable" => "orderitems"
    ),
    "smtp" => array(
        "host" => "",
        "username" => "",
        "password" => "",
        "port" => "465",
        "reply" => ""
    ),
    "pay_api" => "test_j5DVn7SuTCnEh9k7P3n46dfqtrnNyG",
    "debug" => true
);

?>