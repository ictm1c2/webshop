<?php

class Product {

    /*
        Product constructor
    */
    public function __construct($id, $name, $price, $recommendedPrice, $description, $video, $customFields, $tags, $searchDetails, $categories, $stock, $quantity) {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->recommendedPrice = $recommendedPrice;
        $this->description = $description;
        $this->video = $video;
        $this->customFields = $customFields;
        $this->tags = $tags;
        $this->searchDetails = $searchDetails;
        $this->categories = $categories;
        $this->stock = $stock;
        $this->quantity = $quantity;
    }

    /*
        Return a Product object from a query result
    */
    private static function setProperties($result, $quantity) {

        $instance = new self(
            $result->StockItemID,
            $result->StockItemName,
            $result->UnitPrice,
            $result->RecommendedRetailPrice,
            $result->MarketingComments,
            $result->Video,
            $result->CustomFields,
            $result->Tags,
            $result->SearchDetails,
            $result->Categories,
            $result->QuantityOnHand,
            $quantity
        );
        return $instance;
    }

    /*
        Get a product from the database based on it's id
        
        @param int id Product id to queue
        @param int quantity load the product with the quantity, default 1
        
        @return product object
    */
    public static function fromId($id, $quantity = 1) {
        $sql = "SELECT * , GROUP_CONCAT(StockGroupID) AS Categories FROM " . Database::productTable() . " JOIN " . Database::productCategoryTable() . " USING (StockItemID) JOIN " . Database::inventoryTable() . " USING (StockItemID ) WHERE StockItemID=" . $id;
        $result = Database::query($sql);
        return self::setProperties($result[0], $quantity);
    }

    /*
        Get all products for a category, or all products
        
        @param int range Amount of products per page
        @param int page current page, starting at 0, default 0
        @param int category category id, default null

        @return list of product objects
    */
    public static function getProducts($range = 25, $page = 0, $category = null) {
        $sql = "SELECT * , GROUP_CONCAT(StockGroupID) AS Categories FROM " . Database::productTable() 
            . " JOIN " . Database::productCategoryTable() . " USING (StockItemID) JOIN " . Database::inventoryTable() . " USING (StockItemID) GROUP BY StockItemID";

        if ($category != null) {
            $sql .= " HAVING find_in_set(" . $category . ", Categories)";
        }
        $sql .= " LIMIT " . ($page * $range) . "," . $range;

        $result = Database::query($sql);
        $list = array();

        foreach($result as $row) {
            $product = self::setProperties($row);
            array_push($list, $product);
        }

        return $list;
    }

    /*
        Upload a file to the server

        @param $_FILES file post file array

        @return upload message
    */
    public function uploadFile($file) {
        $fileType = $file['type'];
        $fileExt = explode("/",$fileType)[1];

        if ($fileExt != 'png') {
            return "incorrect filetype";
        }

        $path = __DIR__ . "/../images/" . $this->id . "." . $fileExt;
        if (move_uploaded_file($file["tmp_name"], $path)) {
            return true;
        } 
        return false;
    }

    /*
        Update the product
    */
    public function update() {
        // description, foto, video, naam, vooraad
        
        // Update product
        $sql = "UPDATE " . Database::productTable() . " SET StockItemName = '$this->name', MarketingComments = '$this->description', Video = '$this->video' WHERE StockItemID = $this->id";
        Database::query($sql);

        // Update storage
        $sql = "UPDATE " . Database::inventoryTable() . " SET QuantityOnHand = $this->quantity WHERE StockItemID = $this->id";
        Database::query($sql);
    }

    public function getImage() {
        $path = dirname(__DIR__ . "/../") . "images/" . $this->id . ".png";
        return $path;
    }

    public function printOut() {
        print("
            <div class=\"product\">
                <div class=\"img\">
                    <img src=\"".$this->getImage()."\">
                </div>
                <div class=\"content\">
                    <div class=\"title\">
                        <h5>".$this->name."</h5>
                    </div>
                    <div class=\"vooraad\">
                        <h5>".$this->stock."</h5>
                    </div>
                    <div class=\"price\">
                        <h5>&euro;".$this->price."</h5>
                    </div>
                    <div class=\"cart\">
                        <a href=\"../includes/addwishlist.inc.php?id=".$this->id."\"><i class=\"icon heart far fa-heart\"></i></a>
                        <a href=\"../includes/addshoppingcart.inc.php?id=".$this->id."\"><i class=\"icon fas fa-cart-plus\"></i></a>
                    </div>
                </div>
            </div>
        ");
    }
}