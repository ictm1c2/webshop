<?php


class coupon
{
    /*
        Create a new coupon
        $code CouponCode
        $active IsActive
        $discount Discount
        $name CouponName

        Returns id of new coupon
     */
    public static function create($code, $active, $discount, $name)
    {
        $sql = 'INSERT INTO coupons VALUES (?, ?, ?, ?)';
        return Database::query($sql, [$code, $active, $discount, $name], TRUE);
    }

    /*
        Read a single coupon
        $code CouponCodee

        Returns all rows of the selected coupon
     */
    public static function read($code)
    {
        $sql = 'SELECT * FROM coupons WHERE CouponCode = ?';
        return Database::query($sql, [$code]);
    }

    /*
        Update an existing coupon
        $code CouponCode
        $discount Discount
        $name CouponName

        returns the id of the updates row
     */
    public static function update($code, $discount, $name)
    {
        $sql = 'UPDATE coupons SET CouponCode = ?, Discount = ?, CouponName = ? WHERE CouponCode = ?';
        return Database::query($sql, [$code, $discount, $name, $code], TRUE);
    }

    /*
        Get a list of all coupons
        $start = 0 Where the rows should start
        $count = 0 Amount of rows

        returns all rows in coupons
     */
    public static function getList($start = 0, $count = 25)
    {
        $sql = 'SELECT * FROM coupons LIMIT ?, ?';
        return Database::query($sql, [$start, $count]);
    }

    /*
        sets the IsActive field in a coupon to 0
        $code CouponCode

        returns the id of last updated row
     */
    public static function deactivate($code)
    {
        $sql = 'UPDATE coupons SET IsActive = 0 WHERE CouponCode = ?';
        return Database::query($sql, [$code], TRUE);
    }

    /*
        sets the IsActive field in a coupon to 1
        $code CouponCode

        returns the id of last updated row
     */
    public static function activate($code)
    {
        $sql = 'UPDATE coupons SET IsActive = 1 WHERE CouponCode = ?';
        return Database::query($sql, [$code], TRUE);
    }
}