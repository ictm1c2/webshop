<?php

class User {

    public function __construct($id, $email, $password, $type, $firstname, $insertion, $lastname, $street, $postalcode, $town) {
        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
        $this->type = $type;
        $this->firstname = $firstname;
        $this->insertion = $insertion;
        $this->lastname = $lastname;
        $this->street = $street;
        $this->postalcode = $postalcode;
        $this->town = $town;
    }

    private static function setProperties($result) {
        $instance = new self(
            $result->UserID,
            $result->Email,
            $result->Password,
            $result->Type,
            $result->FirstName,
            $result->Insertion,
            $result->LastName,
            $result->Street,
            $result->PostalCode,
            $result->Town
        );
        return $instance;
    }

    /*
        Get a user from it's id

        @param int id User id to find

        @return user object
    */
    public static function fromId($id) {
        $sql = "SELECT * FROM " . Database::userTable() . " WHERE UserID = ?";
        $result = Database::query($sql, [$id]);
        return self::setProperties($result[0]);
    }

    /*
        Get the shopping cart form the user

        @return List of product objects in the cart
    */
    public function getShoppingCart() {
        $products = explode(",",ShoppingCart::fromId($this->id)[0]->CartItemQuantity);
        return $this->transformItemList($products);
    }

    /* 
        Get the total price of the shopping cart

        @return string of shopping cart total
    */
    public function getShoppingTotal() {
        $total = 0.00;

        foreach($this->getShoppingCart() as $product) {
            $total += (double) $product->price;
        }

        return strval(sprintf("%.2f", $total));
    }

    public function clearShoppingCart() {
        // TODO: clear shopping cart
    }

    /*
        Get the wishlist from the user

        @return List of product objects in the wishlish
    */
    public function getWishList() {
        $products = explode(",",WishList::fromId($this->id)['CartItemQuantity']);
        return $this->transformItemList($products);
    }

    private function transformItemList($products) {
        $list = array();
        foreach($products as $product) {
            $productInfo = explode("/",$product);

            $list[$productInfo[0]] = Product::fromId($productInfo[1], $productInfo[2]);
        }
        return $list;
    }

    /*
        Create a new user

        @param string email User's email
        @param string password Plain user's password
        @param int type Account type, 1 default, (2 = admin)

        @return User object
    */
    public static function newUser($email, $password, $firstname, $insertion, $lastname, $street, $postalcode, $town, $type = 1) {
        $hash = password_hash($password, PASSWORD_DEFAULT);
        $sql = "INSERT INTO " . Database::userTable() . " VALUES (NULL,?,?,?,?,?,?,?,?,?)";
        $id = Database::query($sql, [$email,$hash,$type,$firstname,$insertion,$lastname,$street,$postalcode,$town], true);
        return self::fromId($id);
    }

    /*
        Start a user (log a user in to the website)
    */
    public function start() {
        $_SESSION['user'] = serialize($this);
    }

    /*
        Get the user who is logged in

        @return User object
    */
    public static function getUser() {
        return isset($_SESSION['user']) ? unserialize($_SESSION['user']) : false;
    }

    /*
        Create a cookie login for the user
    */
    public function toCookie() {
        setcookie(
            "ICTM1C_WEBSHOP_user",
            serialize($this),
            time() + (10 * 365 * 24 * 60 * 60),
            '/'
        );
    }

    /*
        Delete the user account cookie
    */
    public function delCookie() {
        setcookie(
            "ICTM1C_WEBSHOP_user",
            serialize($this),
            time() - (3600),
            '/'
        );
    }

    /*
        Get the user from a cookie

        @return User object
    */
    public static function fromCookie() {
        return isset($_COOKIE['ICTM1C_WEBSHOP_user']) ? unserialize($_COOKIE['ICTM1C_WEBSHOP_user']) : false;
    }
}

?>

