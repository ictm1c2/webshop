<?php

class WishList
{
    /*
        Create a new wishlist item
        $item ItemStockID
        $user UserID
        $quantity Quantity of items in wishlist

        returns the id of inserted row
     */
    public static function create($item, $user, $quantity)
    {
        $sql = 'INSERT INTO wishlist (StockItemID, UserID, Quantity) values (?, ?, ?)';
        return Database::query($sql, [$item, $user, $quantity], true);
    }

    /*
        Read a single wishlist item
        $id ItemListID

        returns the selected row
     */
    public static function read($id)
    {
        $sql = 'SELECT ItemListID, StockItemID, UserID, Quantity FROM wishlist WHERE ItemListID = ?';
        return Database::query($sql, [$id]);
    }

    /*
        Get a list of wishlist items linked to a UserID
        $user UserID

        returns an array of rows
     */
    public static function fromId($user)
    {
        $sql = "SELECT UserID, GROUP_CONCAT(ItemListID, '/', StockItemID, '/', Quantity) as CartItemQuantity FROM wishlist WHERE UserID = ? GROUP BY UserID";
        return Database::query($sql, [$user]);
    }

    /*
        Delete an item from the wishlist
        returns 1 if succesfull
     */
    public static function delete($id)
    {
        $sql = 'DELETE FROM wishlist WHERE ItemListID = ?';
        return Database::query($sql, [$id]);
    }

    /*
        Set the quanttiy of a wishlist item
        $id Id of the wishlist item
        $quantity quantity the wislist item should be set to

        returns the id of the last updated row
     */
    public static function setQuantity($id, $quantity)
    {
        $sql = 'UPDATE wishlist SET Quantity = ? WHERE ItemListID = ?';
        return Database::query($sql, [$quantity, $id], true);
    }

    /*
        Add to the quantity of a wishlist item
        $id Id of the wishlist item
        $quantity quantity that should be added to current

        returns the id of the last updated row
     */
    public static function addQuantity($id, $quantity)
    {
        $sql = 'UPDATE wishlist SET Quantity = Quantity + ? WHERE ItemListID = ?';
        return Database::query($sql, [$quantity, $id], true);
    }

    /*
        Remove from the quantity of a wishlist item
        $id Id of the wishlist item
        $quantity quantity that should be removed from current

        returns the id of the last updated row
     */
    public static function removeQuantity($id, $quantity)
    {
        $sql = 'UPDATE wishlist SET Quantity = Quantity - ? WHERE ItemListID = ?';
        return Database::query($sql, [$quantity, $id], true);
    }
}