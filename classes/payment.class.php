<?php

class Payment {

    public static function order($orderId, $userId) {
        $user = User::fromId($userId);
        try {
            require(dirname(__DIR__ . '..') . '/includes/initmollie.inc.php');
            
            if ($_SERVER["REQUEST_METHOD"] != "POST") {
                $method = $mollie->methods->get(\Mollie\Api\Types\PaymentMethod::IDEAL, ["include" => "issuers"]);
                echo '<form method="post">Select your bank: <select name="issuer">';
                foreach ($method->issuers() as $issuer) {
                    echo '<option value=' . htmlspecialchars($issuer->id) . '>' . htmlspecialchars($issuer->name) . '</option>';
                }
                echo '<option value="">or select later</option>';
                echo '</select><button>OK</button></form>';
                exit;
            }

            $winkelmand = $user->getShoppingCart();

            $tempHost = "http://http://bd598142.ngrok.io/wwi_case/webshop/";
            $host = $tempHost;

            $payment = $mollie->payments->create([
                "amount" => [
                    "currency" => "EUR",
                    "value" => ($user->getShoppingTotal())
                ],
                "description" => "Payment",
                "redirectUrl" => $host,
                "webhookUrl" => $host . "includes/paywebhook.inc.php",
                "method" => \Mollie\Api\Types\PaymentMethod::IDEAL,
                "metadata" => [
                    "order_id" => $orderId,
                ],
                "issuer" => !empty($_POST["issuer"]) ? $_POST["issuer"] : null
            ]);

            // TODO: write to database
            // database_write($orderId, $payment->status);

            header("Location: " . $payment->getCheckoutUrl(), true, 303);
        } catch (\Mollie\Api\Exceptions\ApiException $e) {
            echo "API call failed: " . htmlspecialchars($e->getMessage());
        }
    }



}