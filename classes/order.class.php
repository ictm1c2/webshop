<?php

class Order {

    public function __construct($id, $userId, $total, $status, $orderDate) {
        $this->id = $id;
        $this->user = $userId;
        $this->total = $total;
        $this->status = $status;
        $this->orderDate = $orderDate;
    }

    /*
        Get a order by it's id

        @param int id Id of the order to get

        @return Order object
    */
    public static function fromId($id) {
        $sql = "SELECT * FROM " . Database::ordersTable() . " WHERE OrderID = ?";
        $result = Database::query($sql, [$id])[0];
        return new self(
            $result->OrderID,
            $result->UserID,
            $result->OrderTotal,
            $result->PaymentStatus,
            $result->OrderDate
        );
    }

    /*
        Get all items from the order

        @return product object list
    */
    public function getItems() {
        $sql = "SELECT * FROM " . Database::orderItemTable() . " WHERE OrderID = ?";
        $result = Database::query($sql, [$this->id]);

        $list = array();

        foreach($result as $row) {
            array_push($list, Product::fromId($row->StockItemID));
        }

        return $list;
    }

    /*
        Create a new order

        @param object user User object
        @param string couponCode Used couponcode code

        @return new order id
    */
    public static function newOrder($user, $couponCode = null) {
        // $sql = "INSERT INTO " . Database::ordersTable() . " VALUES (NULL, ?, ?, ?, ?, CURRENT_TIMESTAMP);";
        // $orderId = Database::query($sql, [$user->id, $user->getShoppingTotal(), 2, $couponCode], true);

        // print_r($orderId);
        // exit();

        $orderId = 1;

        $sql = "";
        $parameters = array();
        foreach($user->getShoppingCart() as $product) {
            $sql .= "INSERT INTO " . Database::orderItemTable() . " VALUES (NULL, ?, ?);";
            array_push($parameters, $orderId, $product->id);
        }

        Database::query($sql, $parameters);
        return $orderId;
    }

    /*
        Returnt the status of the order in a string

        @return Return order status in text
    */
    public function getStatus() {
        return PaymentStatus::fromId($this->status);
    }

    /*
        Update the order status

        @param int status Order status as an int
    */
    public function updateStatus($status) {
        $this->status = $status;
        $this->update();
    }

    /*
        Update the order
    */
    public function update() {
        $sql = "UPDATE " . Database::orderTable() . " SET PaymentStatus = ? WHERE OrderID = ?";
        Database::query($sql, [$this->status, $this->id]);
    }

    /* 
        Prompt a payment
    */
    public function pay() {
        $payment = Payment::order($this->id, $this->user);
    }
}