<?php

class Debugger {

    /*
        Set headers for when debugging
    */
    public static function debugHeaders() {
        if (Settings::isDebug()) {
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
        }
    }

    /*
        Print a debug message if this is enabled
    */
    public static function debug($message, $printr = false) {
        if (Settings::isDebug()) {
            if ($printr) {
                print("[DEBUG] ");
                print_r($message);
            } else {
                print("[DEBUG] $message");
            }
        }
    }
}

