<?php

class Category {

    /*
        Category class constructor
    */
    public function __construct($id, $name) {
        $this->id = $id;
        $this->name = $name;
    }

    /*
        Return Category object from a query result
    */
    private static function setProperties($result) {
        $instance = new self(
            $result->StockGroupID,
            $result->StockGroupName
        );
        return $instance;
    }

    /*
        Return category object based on a given id

        @param int id Category id

        @return Category object
    */
    public static function fromId($id) {
        // $sql = "SELECT * FROM ? WHERE StockGroupID = ?";
        // $result = Database::query($sql, array(Database::categoryTable(), $id));

        $sql = "SELECT * FROM " . Database::categoryTable() . " WHERE StockGroupID=" . $id;
        $result = Database::query($sql);
        return self::setProperties($result[0]);
    }

    /*
        Get a list of all categories objects

        @return List of category objects
    */
    public static function getList() {
        // $sql = "SELECT * FROM ?";
        // $result = Database::query($sql, array(Database::categoryTable()));

        $sql = "SELECT * FROM " . Database::categoryTable();
        $result = Database::query($sql);

        $list = array();

        foreach($result as $category) {
            array_push($list,self::setProperties($category));
        }   
        return $list;
    }

}