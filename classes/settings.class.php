<?php

class Settings {

    /*
        Get the settings for the database
    */
    public static function getDatabaseSettings() {
        require(__DIR__ . "/../includes/settings.inc.php");
        return $config['database'];
    }

    /*
        Get the settings for the mail server
    */
    public static function getSMTPSettings() {
        require(__DIR__ . "/../includes/settings.inc.php");
        return $config['smtp'];
    }    

    /*
        Get the setting for debugging
    */
    public static function isDebug() {
        require(__DIR__ . "/../includes/settings.inc.php");
        return $config['debug'];
    }

    public static function getAPIKey() {
        require(__DIR__ . "/../includes/settings.inc.php");
        return $config['pay_api'];
    }
}