<?php

class shoppingcart
{
    /*
     * Create a new shoppingcart row
     * $item StockItemID
     * $user UserID
     * $quantity Quantity
     *
     * returns id of the new row
     */
    public static function create($item, $user, $quantity)
    {
        $sql = 'INSERT INTO shoppingcart (StockItemID, UserID, Quantity) values (?, ?, ?)';
        return Database::query($sql, [$item, $user, $quantity], true);
    }

    /*
     * Read a single shoppingcart item
     * $id ItemListId
     *
     * returns the selected row
     */
    public static function read($id)
    {
        $sql = 'SELECT ItemListID, StockItemID, UserID, Quantity FROM shoppingcart WHERE ItemListID = ?';
        return Database::query($sql, [$id]);
    }

    /*
     * returns a list of shoppingcart rows linked to a user
     * $user UserID
     *
     * returns an array of rows
     */
    public static function fromId($user)
    {
        $sql = "SELECT UserID, GROUP_CONCAT(ItemListID, '/', StockItemID, '/', Quantity) as CartItemQuantity FROM `shoppingcart` WHERE UserID = ? GROUP BY UserID";
        return Database::query($sql, [$user]);
    }

    /*
     * Remove a shoppingcart item
     *
     * returns query result
     */
    public static function delete($id)
    {
        $sql = 'DELETE FROM shoppingcart WHERE ItemListID = ?';
        return Database::query($sql, [$id]);
    }

    /*
     *  Set the quantity of a shopping cart item
     *
     *  Remove if the quantity is 0
     *
     * returns the query result
     */
    public static function setQuantity($id, $quantity)
    {
        if ($quantity == 0)
            return self::delete($id);

        $sql = 'UPDATE shoppingcart SET Quantity = ? WHERE ItemListID = ?';
        return Database::query($sql, [$quantity, $id], true);
    }

    /*
     * Adds to the quantity of a shoppingcart item
     *
     * Returns query result
     */
    public static function addQuantity($id, $quantity)
    {
        $sql = 'UPDATE shoppingcart SET Quantity = Quantity + ? WHERE ItemListID = ?';
        return Database::query($sql, [$quantity, $id], true);
    }

    /*
     * Subtract from the quantity of a shoppingcart item
     *
     * Returns query result
     */
    public static function subtractQuantity($id, $quantity)
    {
        $sql = 'UPDATE shoppingcart SET Quantity = Quantity - ? WHERE ItemListID = ?';
        return Database::query($sql, [$quantity, $id], true);
    }

    /*
     * Clears the shoppingcart of a specific user
     * $user UserID
     *
     * returns query result
     */
    public static function clearCart($user)
    {
        $sql = "DELETE FROM shoppingcart WHERE UserID = ?";
        return Database::query($sql, [$user], TRUE);
    }
}