<?php

class PaymentStatus {

    /*
        0: betaald
        1: open
        2: afwachtend
        3: mislukt
        4: verlopen
        5: geannuleerd
    */

    public static function fromId($id) {
        switch($id) {
            case 0: return "betaald";
            case 1: return "open";
            case 2: return "afwachtend";
            case 3: return "mislukt";
            case 4: return "verlopen";
            case 5: return "geannuleerd";
        }
    }

    // public static function toId($status) {
    //     switch($status) {
    //         case "open": return 0;
    //         case "afwachtend": return 1;
    //         case "mislukt": return 2;
    //         case "verlopen": return 3;
    //         case "geannuleerd": return 4;
    //     }
    // }

}