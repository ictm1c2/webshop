<?php

class Database  {

    /*
        Get database settings
    */
    public static function getSettings() {
        return Settings::getDatabaseSettings();
    }

    /*
        Returns a PDO database connection
    */
    public static function connect() {
        $dsn = 'mysql:host='.self::getSettings()['host'].';dbname='.self::getSettings()['database'].';charset='.self::getSettings()['charset'];

        try {
            $conn = new PDO($dsn, self::getSettings()['username'], self::getSettings()['password']);
        } catch (\PDOException $e) {
            echo "Failed to connect to database: " . $e->getMessage();
            return;
        }

        // If this isnt set all data will be returned twice, we only need it in object from.
        $conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        // If this isnt set pdo will turn $int into '10' instead of 10 during execute, this messes up LIMIT statements.
        $conn->setAttribute( PDO::ATTR_EMULATE_PREPARES, false);

        return $conn;
    }

    /*
        $sql Query to the database
        (Optional) $arguments arguments for the query
        (Optional) $insert Set insert to true if query is an insert or update
        Returns last inserted ID when on an update or insert
        Returns All rows as an array otherwise
    */
    public static function query($sql, $arguments = [], $insert = false) {
        $conn = self::connect();

        $prep = $conn->prepare($sql);

        if ($prep->execute($arguments))
            if ($insert)
                $res = $conn->lastInsertId();
            else
                $res = $prep->fetchAll();
        else
            $res = $prep->errorInfo();

        $conn = null;
        return $res;
    }

    /*
        Return the product table name
    */
    public static function productTable() {
        return self::getSettings()['itemTable'];
    }

    /*
        Return the category table
    */
    public static function categoryTable() {
        return self::getSettings()['groupTable'];
    }

    /*
        Return the connection between products and categories table name
    */
    public static function productCategoryTable() {
        return self::getSettings()['itemgroupTable'];
    }

    /*
        Return the inventory table name
    */
    public static function inventoryTable() {
        return self::getSettings()['inventoryTable'];
    }

    /*
        Return the user table name
    */
    public static function userTable() {
        return self::getSettings()['userTable'];
    }

    /*
        Return the shoppingcart table name
    */
    public static function shoppingCartTable() {
        return self::getSettings()['shoppingCartTable'];
    }

    /*
        Return the orders table name
    */
    public static function ordersTable() {
        return self::getSettings()['ordersTable'];
    }

    /*
        Return the order items table name
    */
    public static function orderItemTable() {
        return self::getSettings()['orderItemTable'];
    }
}