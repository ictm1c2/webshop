<?php include 'header.php'; ?>


<form  class="product" method="post" action="edit_product.php">
<!--Foto weergeven, foto uploaden, beschrijving-->
    <section>
        <div class="productFoto"><img src="PepeSunglasses.gif"></div>
        <div class="fotoUpload"><input type="file" name="foto" accept="image/*"></div>
        <div  class="productBeschrijving"><textarea cols="25" rows="10" name="description"></textarea></div>
    </section>

<!--Titel aanpassen, filmpje wijzigen/weergeven-->
    <section>
        <div class="titel"><input type="text" name="titel"></div>
         <div class="video"><iframe src="https://www.youtube.com/embed/eMJk4y9NGvE"></iframe></div>
    </section>

<!--Voorraad aanpassen, voorraad opslaan, opslaan algemeen-->
    <section>
        <div class="voorraadBeheer"><input type="number" value="voorraadAantal"> <input type="submit" name="voorraadBevestig" value="voorraad aanpassen"></div>
        <div class="opslaan"><input type="submit" name="opslaan" value="opslaan"></div>
    </section>
</form>

<?php include 'footer.php'; ?>